// B"H
/**
 * 
 */

/**
 * @author sem
 *
 */
// public class MenuTester {

// 	/**
// 	 * @param args
// 	 */
// 	public static void main(String[] args) {
// 		// TODO Auto-generated method stub
// 		System.out.println("hi its is working.");
// 	}

// }

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

// B"H
/**
 * @author sem
 *
 */
public class MenuTester {

	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	
	static ResultSet resultSet;
	static int counterof;
	static Connection connection;
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		System.out.println("testing");
		
		String queryString = "SELECT * FROM menu";
		
		connection = DriverManager.getConnection("jdbc:mysql://localhost/menu?user=root&password=root");
		
		System.out.println("conected.");
		
		resultSet = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE)
				.executeQuery(queryString);
		
		printRs(resultSet);
		resultSet.beforeFirst();
		//menu(0);
		
		String insertStatment = "INSERT INTO `menu`.`menu` "
				+ "(`id`,`parent`,`title`,`price`) VALUES (?,?,?,?)";
		
		//insertNewData(insertStatment);
		//printRs(resultSet); 

	}
	
	public static void menu(int choise) throws SQLException {
		
		resultSet.afterLast(); // set resultSet to end to make sure fowling code execute ones.
		
			while(resultSet.next()) {				
				if(resultSet.getString(2) == null) {	
					System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3));
				}
				if(!resultSet.next()) {
					System.out.println(); //print nice
					choise = getIntFromUser("choose number: ");
					System.out.println(); //print nice
				}
			}
		
		resultSet.beforeFirst(); // set resultSet back to start to be able to iterate over it.
		
		while(resultSet.next()) {
			Integer colmunTwo = resultSet.getString(2) == null ? 0 : Integer.parseInt(resultSet.getString(2));

			if(colmunTwo == choise) {
				if(resultSet.getString(4) != null) {
					System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3) + "- price is: " + resultSet.getString(4));
				}else {
					System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3));
					
				}
			}
		}
		System.out.println(); //print nice
		choise = getIntFromUser("choose number: ");
		System.out.println(); //print nice
		
		resultSet.absolute(choise); // set
		if(resultSet.getString(4) == null) menu(choise); //if price is null call self (recursive).
		else {
				System.out.println("you jest purchase : "); //print nice
				printHierarchy(Integer.parseInt(resultSet.getString(2)));
				resultSet.absolute(choise);
				System.out.println(resultSet.getString(3) + " for " + resultSet.getString(4));
				System.out.println("what to do?"); //print nice
			}
	}
	
	public static void printRs(ResultSet rs) throws SQLException {
		System.out.println("--------");
		while(rs.next()) {
			System.out.print("id:\t" + rs.getString(1) + "\t");
			System.out.print("| parent:\t" + rs.getString(2) + "\t");
			System.out.print("| title:\t\t" + rs.getString(3) + "\t\t");
			System.out.print("| price:\t\t" + rs.getString(4) + "\t\t");	
			System.out.println();
		}
		
	}
	
	public static int getIntFromUser(String msg) {
		Scanner input = new Scanner(System.in);
		System.out.println(msg);
		return input.nextInt();
	}
	
	public static void printHierarchy(int id) throws SQLException {
		
		resultSet.absolute(id);
		
		if(resultSet.getString(2) == null) {
			System.out.print(resultSet.getString(3) + " > ");
		}else {
			Integer parentId = Integer.parseInt(resultSet.getString(2));
			printHierarchy(parentId);
			resultSet.absolute(id);
			System.out.print(resultSet.getString(3) + " > ");
		}
	}
	
	private static void insertNewData(String data) throws SQLException {
		//connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE)
		//.executeQuery(data);
		
		PreparedStatement preparedStatement = connection.prepareStatement(data);
		preparedStatement.setString(1, "16");
		preparedStatement.setString(2, "1");
		preparedStatement.setString(3, "Test");
		preparedStatement.setString(4, "111");
		preparedStatement.executeUpdate(); 
	}

}
