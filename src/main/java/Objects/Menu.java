// B"H
/**
 * 
 */
package Objects;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author sem
 *
 */
public class Menu {

	private static ResultSet resultSet;
	private static Connection connection;

	public static boolean conenectToDb(String user, String password, String schema) throws SQLException {
		setConnection(DriverManager.getConnection(
				"jdbc:mysql://localhost/" + schema + "?user=" + user + "&password=" + password));
		if(getConnection() != null) return true;
		else return false;
	}

	/**
	 * @return the connection
	 */
	public static Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public static void setConnection(Connection connection) {
		Menu.connection = connection;
	}

	public static boolean getQueryData(String query) throws SQLException {			
		resultSet = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE)
				.executeQuery(query);
		if(getResultSet() != null) return true;
		else return false;
	}

	/**
	 * @return the resultSet
	 */
	public static ResultSet getResultSet() {
		return resultSet;
	}

	/**
	 * @param resultSet the resultSet to set
	 */
	public static void setResultSet() {
		Menu.resultSet = resultSet;
	}

	public static void printResultSet() throws SQLException {
		resultSet.beforeFirst();
		int numberOfColumn = resultSet.getMetaData().getColumnCount();
		System.out.println("--------");
		while(resultSet.next()) {
			for (int i = 1; i <= numberOfColumn; i++) {
				System.out.print(resultSet.getMetaData().getColumnName(i) + ":\t" + resultSet.getString(i) + "\t\t");
			}
			System.out.println();
		}
	}

	public static void printHierarchy(int id) throws SQLException {
		resultSet.absolute(id);

		if(resultSet.getString(2) == null) {
			System.out.print(resultSet.getString(3) + " > ");
		}else {
			Integer parentId = Integer.parseInt(resultSet.getString(2));
			printHierarchy(parentId);
			resultSet.absolute(id);
			System.out.print(resultSet.getString(3) + " > ");
		}
	}

	private static void insertNewData( String parent, String title, String price) throws SQLException {

		String insertStatment = "INSERT INTO `menu`.`menu` "
				+ "(`parent`,`title`,`price`) VALUES (?,?,?)";

		PreparedStatement preparedStatement = connection.prepareStatement(insertStatment);
		preparedStatement.setString(1, parent);
		preparedStatement.setString(2, title);
		preparedStatement.setString(3, price);
		preparedStatement.executeUpdate(); 

	}

	public static int getIntFromUser(String msg) {
		Scanner input = new Scanner(System.in);
		System.out.println(msg);
		return input.nextInt();
	}

	public static String getStringInput(String msg) {
		Scanner input = new Scanner(System.in);
		System.out.println(msg);
		return input.nextLine();
	}

	public static void showMenu(int choise) throws SQLException {		
		resultSet.afterLast(); // set resultSet to end to make sure fowling code execute ones.

		while(resultSet.next()) {				
			if(resultSet.getString(2) == null) {	
				System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3));
			}
			if(!resultSet.next()) {
				System.out.println(); //print nice
				choise = getIntFromUser("choose number: ");
				System.out.println(); //print nice
			}
		}

		resultSet.beforeFirst(); // set resultSet back to start to be able to iterate over it.
		int parentForGoBack = 0;
		while(resultSet.next()) {
			Integer colmunTwo = resultSet.getString(2) == null ? 0 : Integer.parseInt(resultSet.getString(2));
			if(colmunTwo == choise) {
				if(resultSet.getString(4) != null) {
					System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3) + "- price is: " + resultSet.getString(4));
					parentForGoBack = colmunTwo;
				}else {
					System.out.println(resultSet.getString(1) + ": " + resultSet.getString(3));
					parentForGoBack = colmunTwo;
				}
			}
		}

		System.out.println();
		resultSet.absolute(parentForGoBack);
		if(parentForGoBack != 0)System.out.println(resultSet.getString(2) + ": Go back.");

		System.out.println(); //print nice
		choise = getIntFromUser("choose number: ");
		System.out.println(); //print nice

		resultSet.absolute(choise); // set
		if(resultSet.getString(2) != null)printHierarchy(Integer.parseInt(resultSet.getString(2)));
		System.out.println();
		resultSet.absolute(choise); // set
		if(resultSet.getString(4) == null) showMenu(choise); //if price is null call self (recursive).
		else {
			System.out.println("you jest purchase : "); //print nice
			printHierarchy(Integer.parseInt(resultSet.getString(2)));
			resultSet.absolute(choise);
			System.out.println(resultSet.getString(3) + " for " + resultSet.getString(4) + " : id: " + resultSet.getString(1));
			System.out.println("what to do?"); //print nice 
		}
	}

	public static void insertMenu(String title) throws SQLException {
		insertNewData(null, title, null);
	}

	public static void insertSubMenu(String parentId, String title) throws SQLException {
		insertNewData(parentId, title, null);
	}

	public static void insertProduct(String parentId, String title, String price) throws SQLException {
		insertNewData(parentId, title, price);
	}

	public static void addRow() throws SQLException {

		if(getIntFromUser("to add row Enter : 1"
				+ "\nto quit Enter    : 0"
				+ "\n"
				+ "\nPlease enter your chois.") == 0)return;
		int typeToAdd = getIntFromUser("do you want to a menu a sub menu or product? "
				+ "\nfor menu Enter     : 1"
				+ "\nfor sub menu Enter : 2"
				+ "\nfor product Enter  : 3"
				+ "\nto quit Enter      : 0"
				+ "\n"
				+ "\nPlease enter your chois.");

		switch (typeToAdd) {
		case 1:
			insertMenu(getStringInput("enter menu title:"));
			break;
		case 2:
			insertSubMenu(getStringInput("enter menu parent id:"), getStringInput("enter menu title:"));
			break;
		case 3:
			insertProduct(getStringInput("enter product parent id:"), getStringInput("enter product title:"),
					getStringInput("enter product price:"));
			break;
		default:
			System.out.println("we quit.");
			break;
		}
	}

	public static void whatToDo() throws SQLException {

		System.out.println("\n------------------------------\nwhat do we want to do now?\n");

		int whatToDo = getIntFromUser("to get menu Enter\t\t: 1"
				+ "\nto watch the table Enter\t: 2"
				+ "\nto insert row Enter\t\t: 3"
				+ "\nto quit Enter\t\t\t: 0"
				+ "\n"
				+ "\nPlease enter your chois.");

		switch (whatToDo) {
		case 1:
			showMenu(0);
			whatToDo();
			break;
		case 2:
			printResultSet();
			System.out.println();
			whatToDo();
			break;
		case 3:
			addRow();
			whatToDo();
			break;
		default:
			System.out.println("we quit.");
			break;
		}
	}
}
